class Cta3 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }

    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse(this.querySelector('script[type="application/json"]').text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initCta3()
    }

    initCta3 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: CTA3")
    }

}

window.customElements.define('fir-cta-3', Cta3, { extends: 'section' })
