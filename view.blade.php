<!-- Start CTA3 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A basic call to action with optional title and text. -->
@endif
<section class="{{ $block->classes }} {{ $bg_color }} " is="fir-cta-3" id="{{ $pinecone_id ?? '' }}" data-title="{{ $pinecone_title ?? '' }}" data-observe-resizes>
  <div class="cta-3 {{ $theme }} {{ $text_align }} {{ $flip }}">
      @if($cta_title)
      <h2>{{ $cta_title }}</h2>
      @endif
      @if($cta_text)
      <p>{{ $cta_text }}</p>
      @endif
      @if($cta_link)
      <a class="btn btn--full" href="{{ $cta_link['url'] }}">{{ $cta_link['title'] }}</a>
      @endif
  </div>
</section>
<!-- End CTA3 -->
