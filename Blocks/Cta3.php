<?php

namespace Fir\Pinecones\Cta3\Blocks;

use Log1x\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Fir\Utils\GlobalFields as GlobalFields;
use function Roots\asset;

class Cta3 extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'CTA';

    /**
     * The block view.
     *
     * @var string
     */
    public $view = 'Cta3.view';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = '';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'fir';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'editor-ul';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = ['CTA3'];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => array('wide', 'full', 'other'),
        'align_text' => false,
        'align_content' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $defaults = [
        'content' => [
            'cta_title' => null,
            'cta_text' => null,
            'cta_link' => null,
            'bg_color' => 'white',
        ]
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        $data = $this->parse(get_fields());
        $newData = [
        ];

        return array_merge($data, $newData);
    }

    private function parse($data)
    {
        $data['cta_title'] = ($data['content']['cta_title']) ?: $this->defaults['content']['cta_title'];
        $data['cta_text'] = ($data['content']['cta_text']) ?: $this->defaults['content']['cta_text'];
        $data['cta_link'] = ($data['content']['cta_link']) ?: $this->defaults['content']['cta_link'];
        $data['bg_color'] = ($data['content']['bg_color']) ?: $this->defaults['content']['bg_color'];
        $data['flip'] = $data['options']['flip_horizontal'] ? 'cta-3--flip' : '';
        $data['text_align'] = 'cta-3--' . $data['options']['text_align'];
        $data['theme'] = 'fir--' . $data['options']['theme'];
        return $data;
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {

        $Cta3 = new FieldsBuilder('Cta3');

        $Cta3
            ->addGroup('content', [
                'label' => 'CTA',
                'layout' => 'block'
            ])
                ->addText('cta_title')
                ->addTextarea('cta_text')
                ->addLink('cta_link')
                ->addSelect('bg_color', [
                    'choices' => [
                        'white' => 'White'
                    ]
                ])
            ->endGroup()
            ->addGroup('options', [
                'label' => 'Options',
                'layout' => 'block'
            ])
            ->addFields(GlobalFields::getFields('flipHorizontal'))
            ->addFields(GlobalFields::getFields('textAlign'))
            ->addFields(GlobalFields::getFields('theme'))
            ->addFields(GlobalFields::getFields('hideComponent'))
            ->endGroup();

        return $Cta3->build();
    }

    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        wp_enqueue_style('sage/app.css', asset('styles/fir/Pinecones/Cta3/style.css')->uri(), false, null);
    }
}
