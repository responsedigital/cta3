module.exports = {
    'name'  : 'CTA3',
    'camel' : 'Cta3',
    'slug'  : 'cta-3',
    'dob'   : 'CTA_3_1440',
    'desc'  : 'A basic call to action with optional title and text.',
}